package controller;

import java.util.ArrayList;

import model.data_structures.IntegersBag;
import model.data_structures.NumbersBag;
import model.logic.IntegersBagOperations;
import model.logic.NumbersBagOperations;

public class Controller {

	private static NumbersBagOperations model = new NumbersBagOperations();
	
	
	public static NumbersBag createBag(ArrayList<Number> values)
	{
         return new <Number> NumbersBag(values);		
	}
	
	
	public static Number getMean(NumbersBag bag){
		return model.computeMean(bag);
	}
	
	public static Number getMax(NumbersBag bag){
		return model.getMax(bag);
	}
	
	public static Number getMin(NumbersBag bag){
		return model.getMin(bag);
	}
	
	public static Number getX(NumbersBag bag){
		return model.getX(bag);
	}
	
	public static String getPrimos(NumbersBag bag){
		return model.getPrimos(bag);
	}
	
}
