package model.logic;

import java.util.ArrayList;
import java.util.Iterator;

import model.data_structures.IntegersBag;
import model.data_structures.NumbersBag;
import model.data_structures.NumbersBag;

public class NumbersBagOperations {
	
	public Number computeMean(NumbersBag bag){
		Double mean = (double) 0;
		int length = 0;
		if(bag != null){
			Iterator<Double> iter = bag.getIterator();
			while(iter.hasNext()){
				mean += iter.next();
				length++;
			}
			if( length > 0) mean = mean / length;
		}
		return mean;
	}
	
	
	public Number getMax(NumbersBag bag){
		Double max = Double.MIN_VALUE;
	    Double value;
		if(bag != null){
			Iterator<Double> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				if( max < value){
					max = value;
				}
			}
			
		}
		return max;
	}
	
	public Number getMin(NumbersBag bag)
	{
		Double min = Double.MAX_VALUE;
		Double value;
		if(bag != null)
		{
			Iterator<Double> iter = bag.getIterator();
			while(iter.hasNext())
			{
				value = iter.next();
				if(min > value)
				{
					min = value;
				}
			}
		}
		return min;
	}
	
	public Number getX(NumbersBag bag)
	{
		Double resp = null;
		ArrayList primes = new ArrayList<>();
		if(bag != null)
		{
			Iterator<Double> iter = bag.getIterator();
			while(iter.hasNext())
			{
				Double value = iter.next();
				if(resp == null)
				{
					resp = value;
				}
				else
				{
					resp = resp * value;
				}
			}
		}
		return resp;
	}
	
	public String getPrimos(NumbersBag bag)
	{
		Double primos = (double) 0;
		ArrayList primes = new ArrayList<>();
		if(bag != null)
		{
			Iterator<Double> iter = bag.getIterator();
			while(iter.hasNext())
			{
				Double value = iter.next();
				int a = 0;
				for(int i=1; i<(value+1); i++)
				{
					if(value%i==0)
					{
						a++;
					}
				}
				if(a==2)
				{
					primes.add(value);
					primos++;
				}
				
			}
		}
		return "El n�mero de primos es: "+primos+" y son "+ primes;
	}
}
